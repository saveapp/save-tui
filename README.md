# Save TUI
A Rust based TUI for browsing your saved links from the comfort of your shell.

## Building and Running
You can build the binary using cargo and then copy it somewhere on your `$PATH`:
```shell
cargo build --release
cp target/release/save-tui ~/bin/save
```

## Configuration
For information on how your configuration file should look like, check out the docs for the [save-cli library crate](https://gitlab.com/saveapp/save-cli).

## Usage
The following keybindings are currently supported:
- `j`/`k` for moving down/up in the list of links
- `g`/`G` for jumping to the first/last link
- `y` for copying the currently selected link to your clipboard
- `return` for opening the currently selected link
- `/` to start the search
  - `return` filters links matching your query
  - `esc` ends the search
- `r` to reload the links from your [API](https://gitlab.com/saveapp/save-api)
- `q` to quit

Additionally, you may select a link with a specific ID by typing its ID followed by `G`.
