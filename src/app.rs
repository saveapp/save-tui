use std::error::Error;

use tui::backend::Backend;
use tui::layout::{Alignment, Constraint, Direction, Layout, Rect};
use tui::style::Style;
use tui::widgets::{Block, Borders, Paragraph, SelectableList, Text};
use tui::Frame;

use save;

use crate::constants::{DEFAULT_BG_COLOR, HIGHLIGHT_BG_COLOR};
use crate::constants::{DEFAULT_FG_COLOR, HIGHLIGHT_FG_COLOR};
use crate::util;
use crate::util::{Info, MoveSelection};

pub struct App<'a> {
    #[allow(dead_code)]
    title: &'a str,
    should_quit: bool,
    should_request: bool,
    save_config: save::Config,
    all_links: Vec<save::Link>,
    // the links being displayed
    links: Vec<save::Link>,
    selected_link: usize,
    // info displayed in the bottom info bar
    info: Option<Info>,
    // whether the user is currently typing a search
    is_searching: bool,
    // the search string the user is typing
    search: String,
    // whether the user is currently typing numbers
    is_typing_id: bool,
    // numbers the user is typing
    jump_to_id: String,
}
impl<'a> App<'a> {
    pub fn new(title: &'a str) -> Result<App<'a>, Box<dyn Error>> {
        let all_links: Vec<save::Link> = Vec::new();
        let links = Vec::new();
        let save_config = save::Config::from_file(None)?;
        Ok(App {
            title,
            should_quit: false,
            should_request: true,
            save_config,
            all_links,
            links,
            selected_link: 0,
            info: None,
            is_searching: false,
            search: String::new(),
            is_typing_id: false,
            jump_to_id: String::new(),
        })
    }

    pub fn on_tick(&mut self) {
        if self.should_request {
            self.reload_links_from_api();
        }
    }

    pub fn should_quit(&self) -> bool {
        self.should_quit
    }

    pub fn draw<B: Backend>(&mut self, f: &mut Frame<B>) {
        let layout = Layout::default()
            .constraints([Constraint::Min(0), Constraint::Length(1)].as_ref())
            .direction(Direction::Vertical)
            .split(f.size());
        let links_area = layout[0];
        let info_line_area = layout[1];
        self.draw_saved_links(f, links_area);
        self.draw_info_line(f, info_line_area);
    }

    fn draw_saved_links<B: Backend>(&mut self, f: &mut Frame<B>, area: Rect) {
        let items: Vec<String> = self
            .links
            .iter()
            .map(|link| format!("{: >4}: {}", link.id(), link))
            .collect();
        let mut list = SelectableList::default()
            .block(Block::default().borders(Borders::NONE))
            .items(&items)
            .select(Some(self.selected_link))
            .style(Style::default().fg(DEFAULT_FG_COLOR))
            .highlight_style(
                Style::default()
                    .bg(HIGHLIGHT_BG_COLOR)
                    .fg(HIGHLIGHT_FG_COLOR),
            )
            .highlight_symbol(">>");
        f.render(&mut list, area);
    }

    fn draw_info_line<B: Backend>(&mut self, f: &mut Frame<B>, area: Rect) {
        let text: Vec<Text> = if self.is_searching {
            vec![Text::raw(format!("/{}", &self.search))]
        } else if self.is_typing_id {
            vec![Text::raw(format!("{}", &self.jump_to_id))]
        } else {
            match &self.info {
                Some(info) => vec![Text::styled(info.message(), *info.style())],
                None => Vec::new(),
            }
        };
        let mut paragraph = Paragraph::new(text.iter())
            .alignment(Alignment::Left)
            .style(Style::default().bg(DEFAULT_BG_COLOR))
            .wrap(true);
        f.render(&mut paragraph, area);
    }

    pub fn on_key(&mut self, c: char) {
        if self.is_searching {
            match c {
                '\n' => self.on_return(),
                _ => {
                    self.search = format!("{}{}", &self.search, &c.to_string());
                    self.filter_links();
                }
            }
        } else {
            match c {
                'q' => self.should_quit = true,
                '/' => self.is_searching = true,
                'r' => {
                    self.should_request = true;
                    self.info("Reloading links from API...");
                }
                'j' => self.move_selection(MoveSelection::OneDown),
                'k' => self.move_selection(MoveSelection::OneUp),
                'g' => self.move_selection(MoveSelection::First),
                'G' => {
                    if self.is_typing_id {
                        self.move_selection(MoveSelection::Id(String::from(&self.jump_to_id)));
                        self.jump_to_id = String::new();
                        self.is_typing_id = false;
                    } else {
                        self.move_selection(MoveSelection::Last);
                    }
                }
                'y' => {
                    if let Some(selected) = self.selected_link() {
                        match util::copy_to_clipboard(selected) {
                            Ok(_) => self.info("Copied link to clipboard"),
                            Err(e) => self.warning(&format!("Could not copy to clipboard: {}", e)),
                        };
                    }
                }
                '\n' => {
                    if let Some(selected) = self.selected_link() {
                        match save::util::open_link(&selected, &self.save_config) {
                            Ok(_) => self.info("Opened link"),
                            Err(e) => self.warning(&format!("Unable to open: {}", e)),
                        };
                    };
                }
                '0'..='9' => {
                    self.jump_to_id = format!("{}{}", &self.jump_to_id, &c.to_string());
                    self.is_typing_id = true;
                }
                _ => {}
            }
        }
    }

    pub fn on_escape(&mut self) {
        if self.is_searching {
            self.is_searching = false;
            self.search = String::new();
            self.filter_links();
        } else if self.is_typing_id {
            self.is_typing_id = false;
            self.jump_to_id = String::new();
        }
    }

    pub fn on_backspace(&mut self) {
        if self.is_searching {
            match self.search.pop() {
                Some(_) => {}
                None => self.is_searching = false,
            }
        }
    }

    pub fn on_return(&mut self) {
        self.info(&format!("Only showing links matching '{}'.", self.search));
        self.search = String::new();
        self.is_searching = false;
    }

    fn filter_links(&mut self) {
        self.links = save::util::filter(Some(&self.search), self.all_links.clone());
        // avoid selecting nonexisting index and seeing nothing until scrolling up
        if let Some(max_selection) = max_list_selection(&self.links) {
            if self.selected_link > max_selection {
                self.selected_link = max_selection;
            }
        }
    }

    fn reload_links_from_api(&mut self) {
        self.should_request = false;
        let links = match save::api::get_all_links(&self.save_config) {
            Ok(l) => {
                self.info = None;
                l
            }
            Err(e) => {
                self.error(&format!("Error getting links from API: {}", e));
                Vec::new()
            }
        };
        self.all_links = links.clone();
        self.links = links;
    }

    fn info(&mut self, message: &str) {
        self.info = Some(Info::info(message));
    }

    fn warning(&mut self, message: &str) {
        self.info = Some(Info::warning(message));
    }

    fn error(&mut self, message: &str) {
        self.info = Some(Info::error(message));
    }

    fn move_selection(&mut self, action: MoveSelection) {
        match action {
            MoveSelection::OneDown => {
                if Some(self.selected_link) < max_list_selection(&self.links) {
                    self.selected_link = self.selected_link + 1
                }
            }
            MoveSelection::OneUp => {
                if Some(self.selected_link) > min_list_selection(&self.links) {
                    self.selected_link = self.selected_link - 1
                }
            }
            MoveSelection::First => {
                self.selected_link = match min_list_selection(&self.links) {
                    Some(index) => index,
                    None => 0,
                }
            }
            MoveSelection::Last => {
                self.selected_link = match max_list_selection(&self.links) {
                    Some(index) => index,
                    None => 0,
                }
            }
            MoveSelection::Id(id) => {
                let id: u64 = match id.parse() {
                    Ok(i) => i,
                    Err(e) => {
                        self.warning(&format!("Unable to jump to '{}': {}", id, e));
                        return;
                    }
                };
                match self.links.iter().position(|l| l.id() == id) {
                    Some(i) => self.selected_link = i,
                    None => {
                        self.warning(&format!("Unable to find link with id '{}'.", id));
                    }
                };
            }
        }
    }

    fn selected_link(&self) -> Option<&save::Link> {
        if self.links.is_empty() {
            None
        } else {
            let selected = &self.links[self.selected_link];
            Some(&selected)
        }
    }
}

fn min_list_selection<T>(list: &Vec<T>) -> Option<usize> {
    if list.is_empty() {
        None
    } else {
        Some(0)
    }
}

fn max_list_selection<T>(list: &Vec<T>) -> Option<usize> {
    if list.is_empty() {
        None
    } else {
        Some(list.len() - 1)
    }
}
