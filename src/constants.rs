use tui::style::Color;

pub const TICK_RATE: u64 = 250;

pub const DEFAULT_BG_COLOR: Color = Color::Black;
pub const HIGHLIGHT_BG_COLOR: Color = Color::Gray;
pub const DEFAULT_FG_COLOR: Color = Color::White;
pub const HIGHLIGHT_FG_COLOR: Color = Color::Black;
pub const WARNING_FG_COLOR: Color = Color::Yellow;
pub const ERROR_FG_COLOR: Color = Color::Red;
