mod app;
mod constants;
mod util;

use std::error::Error;
use std::io;
use std::io::Stdout;
use std::time::Duration;

use clap::App as ClapApp;
use clap::{crate_description, crate_name, crate_version};
use termion::event::Key;
use termion::input::MouseTerminal;
use termion::raw::{IntoRawMode, RawTerminal};
use termion::screen::AlternateScreen;
use tui::backend::TermionBackend;
use tui::Terminal;

use crate::app::App;
use crate::constants::TICK_RATE;
use crate::util::{Config, Event, Events};

fn initialize_terminal() -> Result<
    Terminal<TermionBackend<AlternateScreen<MouseTerminal<RawTerminal<Stdout>>>>>,
    Box<dyn Error>,
> {
    let stdout = io::stdout().into_raw_mode()?;
    let stdout = MouseTerminal::from(stdout);
    let stdout = AlternateScreen::from(stdout);
    let backend = TermionBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;
    terminal.hide_cursor()?;
    Ok(terminal)
}

fn main() -> Result<(), Box<dyn Error>> {
    #[allow(unused_variables)]
    let args = ClapApp::new(crate_name!())
        .version(crate_version!())
        .about(crate_description!())
        .get_matches();

    let events = Events::with_config(Config {
        tick_rate: Duration::from_millis(TICK_RATE),
        ..Config::default()
    });

    let mut terminal = initialize_terminal()?;
    let mut app = App::new(crate_name!())?;
    loop {
        terminal.draw(|mut f| app.draw(&mut f))?;

        match events.next()? {
            Event::Input(key) => match key {
                Key::Char(c) => {
                    app.on_key(c);
                }
                Key::Esc => app.on_escape(),
                Key::Backspace => app.on_backspace(),
                _ => {}
            },
            Event::Tick => {
                app.on_tick();
            }
        }
        if app.should_quit() {
            break;
        }
    }

    Ok(())
}
