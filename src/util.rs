use std::error::Error;
use std::io;
use std::sync::mpsc;
use std::thread;
use std::time::Duration;

use save;

use clipboard::{ClipboardContext, ClipboardProvider};
use termion::event::Key;
use termion::input::TermRead;
use tui::style::Style;

use crate::constants::{DEFAULT_BG_COLOR, DEFAULT_FG_COLOR};
use crate::constants::{ERROR_FG_COLOR, WARNING_FG_COLOR};

pub enum MoveSelection {
    OneDown,
    OneUp,
    First,
    Last,
    Id(String),
}

pub fn copy_to_clipboard(link: &save::Link) -> Result<(), Box<dyn Error>> {
    let mut clip: ClipboardContext = ClipboardProvider::new()?;
    clip.set_contents(String::from(link.url()))?;
    Ok(())
}

pub struct Info {
    message: String,
    style: Style,
}
impl Info {
    pub fn message(&self) -> &str {
        &self.message
    }

    pub fn style(&self) -> &Style {
        &self.style
    }

    pub fn info(message: &str) -> Self {
        let message = String::from(message);
        let style = Style::default().bg(DEFAULT_BG_COLOR).fg(DEFAULT_FG_COLOR);
        Info { message, style }
    }

    pub fn warning(message: &str) -> Self {
        let message = String::from(message);
        let style = Style::default().bg(DEFAULT_BG_COLOR).fg(WARNING_FG_COLOR);
        Info { message, style }
    }

    pub fn error(message: &str) -> Self {
        let message = String::from(message);
        let style = Style::default().bg(DEFAULT_BG_COLOR).fg(ERROR_FG_COLOR);
        Info { message, style }
    }
}

pub enum Event<I> {
    Input(I),
    Tick,
}

#[allow(dead_code)]
pub struct Events {
    rx: mpsc::Receiver<Event<Key>>,
    input_handle: thread::JoinHandle<()>,
    tick_handle: thread::JoinHandle<()>,
}

#[derive(Debug, Clone, Copy)]
pub struct Config {
    pub tick_rate: Duration,
}

impl Default for Config {
    fn default() -> Config {
        Config {
            tick_rate: Duration::from_millis(250),
        }
    }
}

impl Events {
    pub fn with_config(config: Config) -> Events {
        let (tx, rx) = mpsc::channel();
        let input_handle = {
            let tx = tx.clone();
            thread::spawn(move || {
                let stdin = io::stdin();
                for evt in stdin.keys() {
                    match evt {
                        Ok(key) => {
                            if let Err(_) = tx.send(Event::Input(key)) {
                                return;
                            }
                        }
                        Err(_) => {}
                    }
                }
            })
        };
        let tick_handle = {
            let tx = tx.clone();
            thread::spawn(move || {
                let tx = tx.clone();
                loop {
                    tx.send(Event::Tick).unwrap();
                    thread::sleep(config.tick_rate);
                }
            })
        };
        Events {
            rx,
            input_handle,
            tick_handle,
        }
    }

    pub fn next(&self) -> Result<Event<Key>, mpsc::RecvError> {
        self.rx.recv()
    }
}
